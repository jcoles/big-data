import pandas as pd
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay, precision_score, recall_score
import matplotlib.pyplot as plt
import collections
from sklearn import tree


df = pd.read_csv("crimes.csv")
df["used_Primary Type_cat"] = df["Primary Type"].astype("category")
df["used_Description_cat"] = df["Description"].astype("category")
df["used_Location Description_cat"] = df["Location Description"].astype("category")
df["used_Arrest_bool"] = df["Arrest"]=="true"
df["used_Domestic_bool"] = df["Domestic"]=="true"
df["used_District_cat"] = df["District"].astype("category")
df["used_Latitude"] = df["Latitude"]
df["used_Longitude"] = df["Longitude"]
df["used_Date"] = pd.to_datetime(df["Date"])
cols = df.columns
for c in cols:
    if "used" not in c:
        df = df.drop(c, axis=1)


one_hot_desc = pd.get_dummies(df["used_Description_cat"])
one_hot_loc_desc = pd.get_dummies(df["used_Location Description_cat"])
one_hot_dist = pd.get_dummies(df["used_District_cat"])

X = df[["used_Location Description_cat","used_Arrest_bool","used_Domestic_bool","used_District_cat","used_Latitude","used_Longitude"]]

X = pd.concat([X,one_hot_loc_desc],axis=1)
X = pd.concat([X,one_hot_dist],axis=1)
X = X.drop("used_Location Description_cat",axis=1)
X = X.drop("used_District_cat",axis=1)
X.columns = X.columns.astype(str)
min_max_scaler = preprocessing.MinMaxScaler()
X = X.fillna(X.mean())

X = min_max_scaler.fit_transform(X)

y = df["used_Primary Type_cat"]

X_train, X_test, y_train, y_test = train_test_split = train_test_split(X,y, test_size=0.2)

clf = LogisticRegression(solver="sag",max_iter=1000,multi_class="multinomial").fit(X_train,y_train)
print("logistic regression accuracy", clf.score(X_test, y_test))
print("logistic regression precision", precision_score(clf.predict(X_test),y_test,average="weighted"))
print("logistic regression recall", recall_score(clf.predict(X_test),y_test,average="weighted",zero_division=0.0))

cm = confusion_matrix(clf.predict(X_test), y_test,labels=clf.classes_)
disp = ConfusionMatrixDisplay(confusion_matrix=cm,  display_labels=clf.classes_)
disp.plot()
plt.xticks(rotation=90)

plt.show()

clf_tree = tree.DecisionTreeClassifier().fit(X_train,y_train)
print("decision tree accuracy", clf_tree.score(X_test, y_test))
print("decision tree precision", precision_score(clf_tree.predict(X_test),y_test,average="weighted"))
print("decision tree recall", recall_score(clf_tree.predict(X_test),y_test,average="weighted",zero_division=0.0))

cm_tree = confusion_matrix(clf_tree.predict(X_test), y_test,labels=clf.classes_)
disp_tree = ConfusionMatrixDisplay(confusion_matrix=cm_tree,  display_labels=clf.classes_)
disp_tree.plot()
plt.xticks(rotation=90)
plt.show()
print(clf.classes_)
